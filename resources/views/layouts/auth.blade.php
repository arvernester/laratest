<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  @include('partials.metadata')
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/') }}"><b>Lara</b>TEST</a>
  </div>

  @yield('content')
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset('vendor/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
@stack('js')
@stack('script')
</body>
</html>
