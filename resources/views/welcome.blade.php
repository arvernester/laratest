@extends('layouts.auth')

@section('content')
<div class="login-box-body">
    <p class="login-box-msg">@lang('user.welcome', ['name' => 'Stranger'])</p>

    <div class="row">
        <div class="col-md-12 text-center">
            <a href="{{ url('login') }}" class="btn btn-primary">@lang('user.btn.login')</a>
            <a href="{{ url('register') }}" class="btn btn-primary">@lang('user.btn.register')</a>
        </div>
    </div>
</div>
@endsection
