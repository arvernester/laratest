@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('user.welcome', ['name' => auth()->user()->name])</h3>
    </div>

    <div class="box-body">
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ App\User::count() }}</h3>
                        <p>@lang('user.total')</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('user.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ App\Laratest\Book::count() }}</h3>
                        <p>@lang('book.total', ['variable' => 'replacement'])</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('book.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>

    </div>

</div>
@endsection
