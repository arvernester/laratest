@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('user.create')</h3>
    </div>

    <form action="{{ route('user.store') }}" method="post" name="form-create">
    {{ csrf_field() }}
    {{ method_field('post') }}

    <div class="box-body">
        @include('contents.users.form')
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">@lang('user.btn.save')</button>
        <a href="{{ route('user.index') }}" class="btn btn-default">@lang('user.btn.back')</a>
    </div>

    </form>

</div>
@endsection
