@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('user.show')</h3>
    </div>

    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                <img src="https://www.gravatar.com/avatar/{{ md5($user->email) }}?s=150">
            </div>
            <div class="col-md-10">
                <table class="table">
                    <tr>
                        <td>@lang('user.form.name')</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td>@lang('user.form.email')</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>@lang('user.form.role')</td>
                        <td>{{ $user->roles->implode('display_name', ', ') }}</td>
                    </tr>
                    <tr>
                        <td>@lang('user.form.created')</td>
                        <td>{{ $user->created_at->format('Y-m-d H:i') }}</td>
                    </tr>
                    <tr>
                        <td>@lang('user.form.updated')</td>
                        <td>{{ $user->updated_at->format('Y-m-d H:i') }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <a href="{{ route('user.index') }}" class="btn btn-primary" type="button" data-toggle="tooltip" data-original-title="@lang('user.btn.edit')">
            <i class="fa fa-edit"></i>
        </a>
        <a href="{{ route('user.index') }}" class="btn btn-default" type="button" data-toggle="tooltip" data-original-title="@lang('user.btn.back')">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>

</div>
@endsection
