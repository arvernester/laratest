@extends('layouts.auth')

@section('content')
<div class="login-box-body">
    <p class="login-box-msg">@lang('user.register')</p>
    <form action="{{ url('register') }}" method="post">
    {{ csrf_field() }}
    {{ method_field('post') }}

        <div class="form-group has-feedback">
            <input id="name" placeholder="@lang('user.form.name', ['variable' => 'replacement'])" type="text" class="form-control" name="name" value="{{ old('name') }}">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('user.form.email')">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
            <input id="password" placeholder="@lang('user.form.password', ['variable' => 'replacement'])" type="password" class="form-control" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
            <input id="password-confirm" placeholder="@lang('user.form.passwordConfirmation', ['variable' => 'replacement'])" type="password" class="form-control" name="password_confirmation">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="row">
            <div class="col-md-4 pull-right">
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    @lang('user.btn.register')
                </button>
            </div>
        </div>

    </form>
    <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" disabled class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
        <a href="#" disabled class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>
    <!-- /.social-auth-links -->
    <a href="{{ url('password/email') }}">I forgot my password</a>
    <br>
    <a href="{{ url('login') }}" class="text-center">@lang('user.login')</a>
</div>

@endsection


@push('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/iCheck/square/blue.css') }}">
@endpush

@push('js')
	<script src="{{ asset('vendor/iCheck/icheck.min.js') }}"></script>
@endpush

@push('script')
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
@endpush
