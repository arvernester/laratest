@extends('layouts.auth')

@section('content')
<div class="login-box-body">
    <p class="login-box-msg">@lang('user.login')</p>
    <form action="{{ url('login') }}" method="post">
    {{ csrf_field() }}
    {{ method_field('post') }}

        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('user.form.email')">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

            <span class="help-block">{{ $errors->first('email') }}</span>
        </div>
        <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" placeholder="@lang('user.form.password')">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

            <span class="help-block">{{ $errors->first('email') }}</span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input name="remember" type="checkbox"> @lang('user.form.remember')
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">@lang('user.btn.login')</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
    <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" disabled class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
        <a href="#" disabled class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>
    <!-- /.social-auth-links -->
    <a href="{{ url('password/email') }}">I forgot my password</a>
    <br>
    <a href="{{ url('register') }}" class="text-center">Register a new membership</a>
</div>

@endsection


@push('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/iCheck/square/blue.css') }}">
@endpush

@push('js')
	<script src="{{ asset('vendor/iCheck/icheck.min.js') }}"></script>
@endpush

@push('script')
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
@endpush
