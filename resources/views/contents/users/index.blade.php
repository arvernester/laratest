@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('user.title')</h3>
    </div>

    <div class="box-body">
        <p>
            <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm" type="button" data-toggle="tooltip" data-original-title="@lang('user.btn.create')">
                <i class="fa fa-plus"></i>
            </a>
        </p>
        <table class="table">
            <thead>
                <tr>
                    <th>@lang('user.form.name')</th>
                    <th>@lang('user.form.email')</th>
                    <th>@lang('user.form.role')</th>
                    <th>@lang('user.form.created')</th>
                    <th>@lang('user.form.updated')</th>
                    <th class="text-right">@lang('user.action')</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->roles->implode('display_name', ', ') }}</td>
                        <td>
                            @if (! empty($user->created_at))
                                {{ $user->created_at->format('Y-m-d H:i') }}
                            @endif
                        </td>
                        <td>
                            @if (! empty($user->updated_at))
                                {{ $user->updated_at->format('Y-m-d H:i') }}
                            @endif
                        </td>
                        <td class="text-right">
                            <a href="{{ route('user.show', ['book' => $user->id]) }}" class="btn btn-default btn-sm" type="button" data-toggle="tooltip" data-original-title="@lang('user.btn.profile')">
                                <i class="fa fa-user"></i>
                            </a>

                            <a href="{{ route('user.edit', ['book' => $user->id]) }}" class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-original-title="@lang('user.btn.edit')">
                                <i class="fa fa-edit"></i>
                            </a>

                            <a href="{{ route('user.destroy', ['book' => $user->id]) }}" class="btn btn-danger btn-sm delete" type="button" data-toggle="tooltip" data-original-title="@lang('user.btn.delete')">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        @if ($users->total() <= 0)
            <div class="alert alert-info">@lang('user.noUser')</div>
        @endif
    </div>

    @if ($users->total() >= $limit)
    <div class="box-footer">
        {{ $users->links() }}
    </div>
    @endif

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('user.deleteConfirm')</h4>
            </div>

            <form action="" method="post" name="form-delete" id="form-delete">
            {{ csrf_field() }}
            {{ method_field('delete') }}

            <div class="modal-body">
                <p>@lang('user.deleteMessage')</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>
                <button type="submit" class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                </button>
            </div>

            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(function(){
            $('a.delete').click(function(){
                $('#modal-delete').modal('show')

                $('#form-delete').attr('action', $(this).attr('href'))

                return false
            })
        })
    </script>
@endpush
