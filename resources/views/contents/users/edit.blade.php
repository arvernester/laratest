@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('user.edit')</h3>
    </div>

    <form action="{{ route('user.update', ['user' => $user->id]) }}" method="post" name="form-edit">
    {{ csrf_field() }}
    {{ method_field('put') }}

    <div class="box-body">
        @include('contents.users.form', compact('user', 'roles'))
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">@lang('user.btn.save')</button>
        <a href="{{ route('user.index') }}" class="btn btn-default">@lang('user.btn.back')</a>
    </div>

    </form>

</div>
@endsection
