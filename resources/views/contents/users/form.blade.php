<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label class="control-label">@lang('user.form.name')</label>
    <input type="text" name="name" class="form-control" value="{{ old('name', isset($user) ? $user->name : '') }}">
    <span class="help-block">{{ $errors->first('name') }}</span>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label class="control-label">@lang('user.form.email')</label>
    <input type="text" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}">
    <span class="help-block">{{ $errors->first('email') }}</span>
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
    <label class="control-label">@lang('user.form.password')</label>
    <input type="password" name="password" class="form-control">
    <span class="help-block">{{ $errors->first('password') }}</span>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label class="control-label">@lang('user.form.role')</label>
    <select name="role" class="form-control">
    	@foreach($roles as $role)
    		<option value="{{ $role->id }}">{{ $role->display_name }}</option>
    	@endforeach
    </select>
    <span class="help-block">{{ $errors->first('name') }}</span>
</div>
