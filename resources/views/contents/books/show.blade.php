@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('book.show')</h3>
    </div>

    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                @if ($book->hasMedia('original'))
                    <img src="{{ $book->firstMedia('original')->getUrl() }}" class="img-responsive img-rounded">
                @else
                    <p>@lang('book.noImage')</p>
                @endif

            </div>
            <div class="col-md-10">
                <table class="table">
                    <tr>
                        <td>@lang('book.form.title')</td>
                        <td>{{ $book->title }}</td>
                    </tr>
                    <tr>
                        <td>@lang('book.form.author')</td>
                        <td>{{ $book->author }}</td>
                    </tr>
                    @if (! empty($book->created_at))
                    <tr>
                        <td>@lang('book.form.created')</td>
                        <td>{{ $book->created_at->format('Y-m-d H:i') }}</td>
                    </tr>
                    @endif

                    @if(! empty($book->updated_at))
                    <tr>
                        <td>@lang('book.form.updated')</td>
                        <td>{{ $book->updated_at->format('Y-m-d H:i') }}</td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <a href="{{ route('book.edit', ['book' => $book->id]) }}" class="btn btn-primary" type="button" data-toggle="tooltip" data-original-title="@lang('book.btn.edit')">
            <i class="fa fa-edit"></i>
        </a>
        <a href="{{ route('book.index') }}" class="btn btn-default" type="button" data-toggle="tooltip" data-original-title="@lang('book.btn.back')">
            <i class="fa fa-arrow-left"></i>
        </a>
    </div>

</div>
@endsection
