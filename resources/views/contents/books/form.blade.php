<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    <label class="control-label">@lang('book.form.title')</label>
    <input type="text" name="title" class="form-control" value="{{ old('title', isset($book) ? $book->title : '') }}">
    <span class="help-block">{{ $errors->first('title') }}</span>
</div>

<div class="form-group {{ $errors->has('author') ? 'has-error' : '' }}">
    <label class="control-label">@lang('book.form.author')</label>
    <input type="text" name="author" class="form-control" value="{{ old('author', isset($book) ? $book->author : '') }}">
    <span class="help-block">{{ $errors->first('author') }}</span>
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
    <label class="control-label">@lang('book.form.image')</label>
    <input type="file" name="image">
    <span class="help-block">{{ $errors->first('image') }}</span>
</div>
