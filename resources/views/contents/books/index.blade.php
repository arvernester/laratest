@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('book.title')</h3>
    </div>

    <div class="box-body">
        <p>
            @if (auth()->user()->can('book-create'))
            <a href="{{ route('book.create') }}" class="btn btn-primary btn-sm" type="button" data-toggle="tooltip" data-original-title="@lang('book.btn.create')">
                <i class="fa fa-plus"></i>
            </a>
            @endif
        </p>
        <table class="table">
            <thead>
                <tr>
                    <th>@lang('book.form.slug')</th>
                    <th>@lang('book.form.title')</th>
                    <th>@lang('book.form.author')</th>
                    <th>@lang('book.form.created')</th>
                    <th>@lang('book.form.updated')</th>
                    <th class="text-right">@lang('book.action')</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($books as $book)
                    <tr>
                        <td>{{ $book->slug }}</td>
                        <td>
                            <a href="#" class="book-title" data-name="title" data-value="{{ $book->title }}" data-type="text" data-pk="{{ $book->id }}" data-url="{{ route('book.updateable', ['book' => $book->id]) }}" data-title="@lang('book.form.title')">
                                {{ $book->title }}
                            </a>
                        </td>
                        <td>
                            <a href="#" class="book-author" data-name="author" data-value="{{ $book->author }}" data-type="text" data-pk="{{ $book->id }}" data-url="{{ route('book.updateable', ['book' => $book->id]) }}" data-title="@lang('book.form.author')">
                                {{ $book->author }}
                            </a>
                        </td>
                        <td>
                            @if (! empty($book->created_at))
                                {{ $book->created_at->format('Y-m-d H:i') }}
                            @endif
                        </td>
                        <td class="updated-{{ $book->id }}">
                            @if (! empty($book->updated_at))
                                {{ $book->updated_at->format('Y-m-d H:i') }}
                            @endif
                        </td>
                        <td class="text-right">
                            <a href="{{ route('book.upload', ['book' => $book->id]) }}" class="btn btn-default btn-sm upload" type="button" data-toggle="tooltip" data-original-title="@lang('book.btn.uploadImage')" data-url="{{ route('book.show', ['book' => $book->id]) }}">
                                <i class="fa fa-upload"></i>
                            </a>

                            @if (auth()->user()->can('book-show'))
                            <a href="{{ route('book.show', ['book' => $book->id]) }}" class="btn btn-default btn-sm" type="button" data-toggle="tooltip" data-original-title="@lang('book.btn.show')">
                                <i class="fa fa-book"></i>
                            </a>
                            @endif

                            @if (auth()->user()->can('book-update'))
                            <a href="{{ route('book.edit', ['book' => $book->id]) }}" class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-original-title="@lang('book.btn.edit')">
                                <i class="fa fa-edit"></i>
                            </a>
                            @endif

                            @if (auth()->user()->can('book-delete'))
                            <a href="{{ route('book.destroy', ['book' => $book->id]) }}" class="btn btn-danger btn-sm delete" type="button" data-toggle="tooltip" data-original-title="@lang('book.btn.delete')">
                                <i class="fa fa-trash"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        @if ($books->total() <= 0)
            <div class="alert alert-info">@lang('book.noBook')</div>
        @endif
    </div>

    @if ($books->total() >= $limit)
    <div class="box-footer">
        {{ $books->links() }}
    </div>
    @endif

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('book.deleteConfirm')</h4>
            </div>

            <form action="" method="post" name="form-delete" id="form-delete">
            {{ csrf_field() }}
            {{ method_field('delete') }}

            <div class="modal-body">
                <p>@lang('book.deleteMessage')</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>
                <button type="submit" class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                </button>
            </div>

            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-upload">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('book.uploadConfirm')</h4>
            </div>

            <form action="" method="post" name="form-upload" id="form-upload" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('post') }}

            <div class="modal-body">
                <div class="validation"></div>
                <div class="image-thumbnail"></div>

                <div class="form-group">
                    <label class="control-label">@lang('book.form.image')</label>
                    <input type="file" name="image" id="cover-image">
                </div>
            </div>

            <div class="modal-footer">
                <i class="fa fa-spinner fa-spin preloader"></i>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-upload"></i>
                </button>
            </div>

            </form>
        </div>
    </div>
</div>
@endsection

@push('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
@endpush

@push('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
@endpush

@push('script')
    <script>
        $(function(){
            $('.preloader, .validation').hide()

            $('.book-title, .book-author').editable({
                send: 'always',
                ajaxOptions: {
                    type: 'put'
                },
                params: function(params) {
                    params._token = '{{ csrf_token() }}'

                    return params
                },
                success: function(response) {
                    if (response.isSuccess == false) {
                        return response.message
                    }

                    $('.updated-'+response.content.id).text(response.content.updatedAt)
                }
            })

            $('a.delete').click(function(){
                $('#modal-delete').modal('show')

                $('#form-delete').attr('action', $(this).attr('href'))

                return false
            })

            $('a.upload').click(function(){
                $('.image-thumbnail').empty()
                $('.validation').hide()

                $.get($(this).attr('data-url'), null, function(response){
                    if (response.isSuccess == true && response.content.image != '') {
                        $('.image-thumbnail')
                            .html('<figure><img src="'+response.content.image+'" alt="'+response.content.title+'" class="img-responsive"> <figcaption>'+response.content.title+'</figcaption></figure><hr>')
                            .show()
                    }
                })

                $('#modal-upload').modal('show')

                $('#form-upload').attr('action', $(this).attr('href'))

                return false
            })

            $('#form-upload').submit(function(){
                var file = $('#cover-image').prop('files')[0]
                var data = new FormData();
                data.append('_token', '{{ csrf_token() }}')
                data.append('image', file)

                $('.preloader').show()

                $.ajax({
                    url: $(this).attr('action'),
                    type: $(this).attr('method'),
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(response, status, xhr) {
                        if (response.isSuccess == true) {
                            $('.validation')
                                .text('')
                                .addClass('alert alert-success')
                                .text(response.message)
                                .show()

                            $('.image-thumbnail')
                                .html('<figure><img src="'+response.content.image+'" alt="'+response.content.title+'" class="img-responsive"> <figcaption>'+response.content.title+'</figcaption></figure><hr>')
                            .show()
                        }
                        else {
                            $('.validation')
                                .text('')
                                .addClass('alert alert-danger')
                                .text('@lang('book.message.error')')
                                .show()
                        }

                        $('.preloader').hide()
                    }
                })

                return false
            })
        })
    </script>
@endpush
