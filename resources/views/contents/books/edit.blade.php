@extends('layouts.default')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('book.edit')</h3>
    </div>

    <form action="{{ route('book.update', ['book' => $book->id]) }}" method="post" name="form-edit"  enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('put') }}

    <div class="box-body">
        @include('contents.books.form', compact('book'))
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">@lang('book.btn.save')</button>
        <a href="{{ route('book.index') }}" class="btn btn-default">@lang('book.btn.back')</a>
    </div>

    </form>

</div>
@endsection
