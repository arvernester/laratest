<?php

return [
    'title'            => 'Users',
    'subtitle'         => 'Management all users',
    'noUser'           => 'No available users.',
    'action'           => 'Actions',
    'create'           => 'Create new user',
    'edit'             => 'Edit user',
    'show'             => 'Show user detail',
    'permissionDenied' => 'You don\'t has permission to access user page.',
    'welcome'          => 'Welcome, :name',
    'home'             => 'Home',
    'total'            => 'Total User',
    'register'         => 'Register as User',
    'login'            => 'Login as User',
    'index'            => 'User Index',

    'form'             => [
        'name'                 => 'Name',
        'email'                => 'Email Address',
        'created'              => 'Created',
        'updated'              => 'Updated',
        'role'                 => 'Role',
        'password'             => 'Password',
        'passwordConfirmation' => 'Password Confirmation',
        'remember'             => 'Remember Me',
    ],

    'btn'              => [
        'create'   => 'Create',
        'save'     => 'Save',
        'back'     => 'Back',
        'profile'  => 'Profile',
        'edit'     => 'Edit',
        'delete'   => 'Delete',
        'register' => 'Register',
        'logout'   => 'Logout',
        'profile'  => 'Profile',
        'login'    => 'Log In',
    ],

    'message'          => [
        'created' => 'User named :name has been created.',
        'edited'  => 'User has been edited.',
        'deleted' => 'User has been deleted.',
    ],
];
