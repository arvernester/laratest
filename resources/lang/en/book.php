<?php

return [
    'title'            => 'Book',
    'subtitle'         => 'Manage published and drafted books',
    'action'           => 'Actions',
    'create'           => 'Create a New Book',
    'deleteConfirm'    => 'Delete Confirmation',
    'deleteMessage'    => 'Are you sure want tho delete this book? This action can\'t be undone.',
    'resultFor'        => 'Search results for ":query"',
    'noBook'           => 'No available books.',
    'permissionDenied' => 'You don\'t has permission to access book page.',
    'total'            => 'Total Book',
    'noImage'          => 'No Image Available',
    'uploadConfirm'    => 'Upload Form',
    'index'            => 'Book Index',

    'form'             => [
        'slug'    => 'Slug',
        'title'   => 'Title',
        'author'  => 'Author',
        'created' => 'Created',
        'updated' => 'Updated',
        'image'   => 'Cover Image',
    ],

    'btn'              => [
        'create'      => 'Create a book',
        'edit'        => 'Edit book',
        'delete'      => 'Delete book',
        'save'        => 'Save',
        'back'        => 'Back',
        'close'       => 'Close',
        'show'        => 'Show book detail',
        'uploadImage' => 'Upload cover image',
    ],

    'message'          => [
        'created'          => 'A new book titled ":title" has been created.',
        'updated'          => 'Book ":title" has been updated.',
        'deleted'          => 'Book has been deleted.',
        'error'            => 'Internal Server Error.',
        'uploaded'         => 'Image for ":title" has been uploaded successfully.',
        'fieldUnavailable' => 'Field ":field" is not exists.',
    ],
];
