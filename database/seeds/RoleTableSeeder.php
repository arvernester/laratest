<?php

use App\Laratest\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role;

        $roles = [
            'user'        => [
                'name'         => 'user',
                'display_name' => 'User',
                'description'  => 'User and author',
            ],
            'admin'       => [
                'name'         => 'admin',
                'display_name' => 'Admin',
                'description'  => 'User and book moderator',
            ],
            'super_admin' => [
                'name'         => 'super_admin',
                'display_name' => 'Super Admin',
                'description'  => 'Super Administrator to rule them all',
            ],
        ];

        $role->insert($roles);
    }
}
