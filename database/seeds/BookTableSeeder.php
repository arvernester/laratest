<?php

use App\Laratest\Book;
use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
            [
                'slug'   => 'the-hunger-games',
                'title'  => 'The Hunger Games',
                'author' => 'Suzanne Collins',
            ],
            [
                'slug'   => 'to-kill-a-mockingbird',
                'title'  => 'To Kill a Mockingbird',
                'author' => 'Harper Lee',
            ],
            [
                'slug'   => 'pride-and-prejudice',
                'title'  => 'Pride and Prejudice',
                'author' => 'Jane Austen',
            ],
            [
                'slug'   => 'the-book-thief',
                'title'  => 'The Book Thief',
                'author' => 'Markus Zusak',
            ],

            [
                'slug'   => 'animal-farm',
                'title'  => 'Animal Farm',
                'author' => 'George Orwell',
            ],
        ];

        $book = new Book;
        $book->insert($books);
    }
}
