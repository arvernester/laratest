<?php

use App\Laratest\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleSuperAdmin = Role::whereName('super_admin')->first();
        $userSuperAdmin = User::create([
            'name'       => 'Super Admin',
            'email'      => 'superadmin@email.com',
            'password'   => bcrypt('password'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $userSuperAdmin->attachRole($roleSuperAdmin);

        $roleAdmin = Role::whereName('admin')->first();
        $userAdmin = User::create([
            'name'       => 'Admin',
            'email'      => 'admin@email.com',
            'password'   => bcrypt('password'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $userAdmin->attachRole($roleAdmin);

        $roleUser = Role::whereName('user')->first();
        $user = User::create([
            'name'       => 'User',
            'email'      => 'user@email.com',
            'password'   => bcrypt('password'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $user->attachRole($roleUser);
    }
}
