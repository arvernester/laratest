<?php

use App\Laratest\Permission;
use App\Laratest\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // book permissions
        $bookCreate = Permission::create([
            'name'         => 'book-create',
            'display_name' => 'Create Book',
            'description'  => 'Create or publish new book',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        $bookUpdate = Permission::create([
            'name'         => 'book-update',
            'display_name' => 'Update Book',
            'description'  => 'Update existing book',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        $bookShow = Permission::create([
            'name'         => 'book-show',
            'display_name' => 'Show Book',
            'description'  => 'Show existing book',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        $bookDelete = Permission::create([
            'name'         => 'book-delete',
            'display_name' => 'Delete Book',
            'description'  => 'Delete existing book',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        // user permissions
        $userCreate = Permission::create([
            'name'         => 'user-create',
            'display_name' => 'Create User',
            'description'  => 'Create or publish new user',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        $userUpdate = Permission::create([
            'name'         => 'user-update',
            'display_name' => 'Update User',
            'description'  => 'Update existing user',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        $userShow = Permission::create([
            'name'         => 'user-show',
            'display_name' => 'Show User',
            'description'  => 'Show existing user',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        $userDelete = Permission::create([
            'name'         => 'user-delete',
            'display_name' => 'Delete User',
            'description'  => 'Delete existing user',
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now(),
        ]);

        $superAdmin = Role::whereName('super_admin')->first();
        $admin = Role::whereName('admin')->first();
        $user = Role::whereName('user')->first();

        $superAdmin->attachPermission($bookCreate);
        $superAdmin->attachPermission($bookUpdate);
        $superAdmin->attachPermission($bookShow);
        $superAdmin->attachPermission($bookDelete);
        $superAdmin->attachPermission($userCreate);
        $superAdmin->attachPermission($userUpdate);
        $superAdmin->attachPermission($userShow);
        $superAdmin->attachPermission($userDelete);

        $admin->attachPermission($bookCreate);
        $admin->attachPermission($bookUpdate);
        $admin->attachPermission($bookShow);

        $user->attachPermission($bookCreate);
    }
}
