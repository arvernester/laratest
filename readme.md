# Laratest

Laratest is simple book management application built-with [Laravel Framework](https://www.laravel.com). This application is not production ready. It's just sample application for test porpuse.

## Requirements

- Laravel 5.2.*
- OpenSSL PHP Extension
- MySQL Server
- PHP >= 5.5.9
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension

## Installation

- Clone this Git repository into your local environment.
- Copy file ```.env.example``` to ```.env```.
- Generate new ky by typing ```php artisan key:generate``` on command line terminal.
- Install framework and it's dependencies by typing ```composer update``` on command line terminal.
- Change database setting value on file ```.env``` based on your local environment.
- Run migration and sample data by typing ```php artisan migrate --seed``` on command line terminal.
- Run built-in web server by typing ```php artisan serve``` or ```php -S localhost:8000 -t public/``` on root directory.
- Access application via browser by typing ```http://localhost:8000```.

By default, you can use email ```superadmin@email.com``` and password ```password``` to login as Super Administrator.

For login as user, use email ```user@email``` and use email ```admin@email.com``` if you want to login as Admin. All account has same password with Super Admin Account. Or, you can create it with Super Admin by login to dashboard page.
