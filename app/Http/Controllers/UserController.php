<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\ValidationRequest;
use App\Laratest\Role;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        abort_if(!\Auth::user()->hasRole('super_admin'), 500, trans('user.permissionDenied'));
    }

    /**
     * Display a listing of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name', 'ASC')
            ->when(request('q'), function ($query) {
                return $query->where('name', 'LIKE', '%' . request('q') . '%');
            })
            ->paginate($limit = 20);

        return view('contents.users.index', compact('users'))
            ->withTitle(trans('user.title'))
            ->withSubTitle(trans('user.subtitle'))
            ->withLimit($limit)
            ->withSearch(true);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::orderBy('display_name', 'ASC')->get();

        return view('contents.users.create', compact('roles'))
            ->withTitle(trans('user.create'));
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidationRequest $request)
    {
        $role = Role::findOrFail($request->role);

        $user = \DB::transaction(function () use ($request, $role) {
            $user = new User;

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->remember_token = $request->_token;

            $user->save();

            // attach to role
            $user->attachRole($role);

            return $user;
        });

        return redirect()
            ->route('user.index')
            ->withSuccess(trans('user.message.created', ['name' => $user->name]));
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('contents.users.show', compact('user'))
            ->withTitle($user->name);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::orderBy('display_name')->get();

        return view('contents.users.edit', compact('user', 'roles'))
            ->withTitle(trans('user.edit'));
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidationRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $role = Role::findOrFail($request->role);

        \DB::transaction(function () use ($user, $role, $request) {
            $user->name = $request->name;

            $user->save();

            // syn roles
            $user->roles()->sync([$role->id]);
        });

        return redirect()
            ->route('user.index')
            ->withSuccess(trans('user.message.edited'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->delete() === true) {
            return redirect()
                ->route('user.index')
                ->withSuccess(trans('user.message.deleted'));
        }

        return redirect()->back();
    }
}
