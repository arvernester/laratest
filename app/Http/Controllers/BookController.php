<?php

namespace App\Http\Controllers;

use App\Http\Requests\Book\UpdateableRequest;
use App\Http\Requests\Book\UploadRequest;
use App\Http\Requests\Book\ValidationRequest;
use App\Laratest\Book;
use Auth;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function __construct()
    {
        abort_if(!Auth::user()->hasRole(['super_admin', 'admin', 'user']), 500, trans('book.permissionDenied'));
    }

    /**
     * Display a listing of the book.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(!Auth::user()->can('book-show'), 500, trans('book.permissionDenied'));

        $books = Book::orderBy('title', 'ASC')
            ->when(request('q'), function ($query) {
                return $query->where('title', 'LIKE', '%' . request('q') . '%')
                    ->orWhere('author', 'LIKE', '%' . request('q') . '%');
            })
            ->paginate($limit = 20);

        return view('contents.books.index', compact('books'))
            ->withLimit($limit)
            ->withTitle(empty(request('q')) ? trans('book.title') : trans('book.resultFor', ['query' => request('q')]))
            ->withSubTitle(trans('book.subtitle'))
            ->withSearch(true);
    }

    /**
     * Show the form for creating a new book.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(!Auth::user()->can('book-create'), 500, trans('book.permissionDenied'));

        return view('contents.books.create')
            ->withTitle(trans('book.create'));
    }

    /**
     * Store a newly created book in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidationRequest $request)
    {
        abort_if(!Auth::user()->can('book-create'), 500, trans('book.permissionDenied'));

        $book = \DB::transaction(function () use ($request) {
            $book = new Book;

            $book->slug = str_slug($request->title);
            $book->title = $request->title;
            $book->author = $request->author;

            $book->save();

            if (!empty($request->image) and $request->file('image')->isValid()) {
                $image = $this->uploadImage($book, $request);

                $book->attachMedia($image, ['original', 'thumbnail']);
            }

            return $book;
        });

        return redirect()
            ->route(Auth::user()->can('book-show') ? 'book.index' : 'book.create')
            ->withSuccess(trans('book.message.created', ['title' => $book->title]));
    }

    /**
     * Display the specified book.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort_if(!Auth::user()->can('book-show'), 500, trans('book.permissionDenied'));

        $book = Book::findOrFail($id);

        if (request()->ajax()) {
            return [
                'isSuccess' => true,
                'content'   => [
                    'id'     => $book->id,
                    'title'  => $book->title,
                    'author' => $book->author,
                    'image'  => $book->hasMedia('thumbnail') ? $book->firstMedia('thumbnail')->getUrl() : '',
                ],
            ];
        }

        return view('contents.books.show', compact('book'))
            ->withTitle($book->title)
            ->withSubTitle(trans('book.writenBy', ['author' => $book->author]));
    }

    /**
     * Show the form for editing the specified book.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_if(!Auth::user()->can('book-update'), 500, trans('book.permissionDenied'));

        $book = Book::findOrFail($id);

        return view('contents.books.edit', compact('book'))
            ->withTitle($book->title)
            ->withSubTitle(trans('book.writenBy', ['author' => $book->author]));
    }

    /**
     * Update the specified book in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidationRequest $request, $id)
    {
        abort_if(!Auth::user()->can('book-update'), 500, trans('book.permissionDenied'));

        $book = Book::findOrFail($id);

        $book = \DB::transaction(function () use ($book, $request) {
            $book->title = $request->title;
            $book->author = $request->author;

            $book->save();

            if (!empty($request->image) and $request->file('image')->isValid()) {
                $image = $this->uploadImage($book, $request);

                // sync media
                $book->syncMedia($image, ['original', 'thumbnail']);
            }

            return $book;
        });

        return redirect(route('book.edit', ['book' => $book->id]))
            ->withSuccess(trans('book.message.updated', ['title' => $book->title]));
    }

    /**
     * Remove the specified book from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_if(!Auth::user()->can('book-delete'), 500, trans('book.permissionDenied'));

        $book = Book::findOrFail($id);

        if ($book->delete() === true) {
            return redirect()
                ->route('book.index')
                ->withSuccess(trans('book.message.deleted'));
        }

        return redirect()->back();
    }

    /**
     * @param UploadRequest $request
     * @param $id
     */
    public function upload(UploadRequest $request, $id)
    {
        $book = Book::findOrFail($id);

        // upload cover image
        $cover = $this->uploadImage($book, $request);

        // attach to book
        $book->syncMedia($cover, ['original', 'thumbnail']);

        return [
            'isSuccess' => true,
            'message'   => trans('book.message.uploaded', ['title' => $book->title]),
            'content'   => [
                'title' => $book->title,
                'image' => $book->firstMedia('thumbnail')->getUrl(),
            ],
        ];
    }

    /**
     * @param UpdateableRequest $request
     * @param $id
     */
    public function updateable(UpdateableRequest $request, $id)
    {
        $book = Book::findOrFail($id);

        $column = $request->name;

        $columns = collect($book->getAttributes())->only(['title', 'author']);

        if (!$columns->has($column)) {
            return [
                'isSuccess' => false,
                'message'   => trans('book.message.fieldUnavailable', ['field' => $column]),
            ];
        }

        $book->$column = $request->value;

        return [
            'isSuccess' => $book->save(),
            'message'   => trans('book.message.updated'),
            'content'   => [
                'id'        => $book->id,
                'updatedAt' => $book->updated_at->format('Y-m-d H:i'),
            ],
        ];
    }

    /**
     * @param $book
     * @param $request
     * @return mixed
     */
    private function uploadImage($book, $request)
    {
        $image = \Media::fromSource($request->file('image'))
            ->useFilename($book->slug)
            ->toDestination('media', 'book')
            ->upload();

        if (request()->isMethod('put')) {
            \Media::update($image);
        }

        return $image;
    }
}
