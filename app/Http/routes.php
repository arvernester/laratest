<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
    if (\Auth::check()) {
        return redirect()->route('dashboard');
    }

    return view('welcome')
        ->withTitle('Laratest');
});

Route::get('test', function () {
    return view('test');
});

Route::auth();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
    Route::put('book/{book}/updateable', 'BookController@updateable')->name('book.updateable');
    Route::post('book/upload/{book}', 'BookController@upload')->name('book.upload');
    Route::resource('book', 'BookController');

    Route::resource('user', 'UserController');
});
