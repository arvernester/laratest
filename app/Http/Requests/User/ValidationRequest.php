<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Laratest\Role;

class ValidationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = Role::select('id')->get()->implode('id', ',');

        $rules = [
            'name' => 'required|max:100',
            'role' => 'required|integer|in:' . $roles,
        ];

        if (request()->isMethod('post')) {
            $rules['email'] = 'required|email|max:100|unique:users,email';
        }

        return $rules;
    }
}
