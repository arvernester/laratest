<?php

namespace App\Http\Requests\Book;

use App\Http\Requests\Request;

class UpdateableRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'in:title,author',
        ];

        if (request()->has('title')) {
            $rules['title'] = 'required|max:200';
        }

        if (request()->has('author')) {
            $rules['author'] = 'required|max:100';
        }

        return $rules;
    }
}
