<?php

namespace App\Http\Requests\Book;

use App\Http\Requests\Request;

class ValidationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'  => 'required|max:200',
            'author' => 'required|max:100',
        ];

        return $rules;
    }
}
