<?php

namespace App\Laratest;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'display_name',
        'description',
        'created_at',
        'updated_at',
    ];
}
