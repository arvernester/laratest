<?php

namespace App\Laratest;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Book extends Model
{
    use Mediable;

    /**
     * @var array
     */
    protected $fillable = [
        'slug',
        'title',
        'author',
        'created_at',
        'updated_at',
    ];
}
