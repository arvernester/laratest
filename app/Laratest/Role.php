<?php

namespace App\Laratest;

use App\User;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * @return mixed
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
